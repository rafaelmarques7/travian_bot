This is a BOT for the online game Travian. 

It emulates the game patterns of a human player:
    - evolve crops and buildings,
    - construct troops
    - send and distribute raids
    - escape from incomings attacks
    - send hero to adventures
    ...

Note: this violates the Terms of Service, and thus, you can be banned by using it. 